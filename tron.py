# gpn-tron-cli by nols

import socket
import logging
import time
import re
import random

# SERVER_HOSTNAME = "gpn-tron.duckdns.org"
# SERVER_HOSTNAME = "151.216.77.207"
# SERVER_HOSTNAME = "gpn-tron.alliance.rocks"
SERVER_HOSTNAME = "localhost"

SERVER_PORT = 4000
PACKAGE_RECV_SIZE = 2048
PASSWORD_FILE_PATH = "password.txt"
# LOG_LVL = "INFO"
# PACKET_TYPES = ['motd', 'join', 'game', 'error', 'pos', 'player',
#                 'tick', 'die', 'move', 'chat', 'message', 'win', 'lose']
PACKET_SERVER_TYPES = ['motd', 'game', 'error', 'pos', 'player',
                       'tick', 'die', 'message', 'win', 'lose']

CLIENT_USERNAME = "nolsbot"
CLIENT_PASSWORD = ""


game = {}
players = {}
pos = {}
dead_players = []
players_shown = False


def read_password():
    f = open(PASSWORD_FILE_PATH, "r")
    if f is None:
        logger.fatal(f"could not open password file at {PASSWORD_FILE_PATH}")
    global CLIENT_PASSWORD
    CLIENT_PASSWORD = f.readline().strip()


def package_send(package_type, args):
    package = f"{package_type}|{args}\n"
    sock.sendall(package.encode())
    logger.info(f"Send: {package.encode()}")


def package_parse():
    global packages_parsed
    response = sock.recv(PACKAGE_RECV_SIZE)
    print(f"response: {response}")
    packages = response.split(b"\n")
    packages.pop(-1)
    packages_parsed = []
    for package in packages:
        packages_parsed.append(package.split(b"|"))
    # logger.in fo(packages_parsed)


# def build_matrix():
#     global game_matrix
#     game_matrix = []
#     # for player in pos:
#     #     game_matrix


def move():
    package_send("move", random.choice(['left', 'up']))


if __name__ == "__main__":
    # init logger
    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # resolve hostname to ip if not already ip
    if re.match("(\b25[0-5]|\b2[0-4][0-9]|\b[01]?[0-9][0-9]?)(.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}", SERVER_HOSTNAME):
        server_ip = SERVER_HOSTNAME
        SERVER_HOSTNAME = socket.gethostbyaddr(server_ip)
    else:
        server_ip = socket.gethostbyname(SERVER_HOSTNAME)

    logger.info(f"{SERVER_HOSTNAME=}")
    logger.info(f"{server_ip=}")

    # read client password
    read_password()

    # connect to server
    sock.connect((server_ip, SERVER_PORT))
    if sock is None:
        logger.error("could not connect")

    # gameloop
    while True:
        package_parse()
        if players_shown == False:
            logger.info(players)
            players_shown = True

        for pack in packages_parsed:
            if pack[0].decode() in PACKET_SERVER_TYPES:
                if pack[0].decode() == 'motd':
                    logging.info(f"MOTD: {pack[1].decode()}")
                    # join game
                    package_send('join', f"{CLIENT_USERNAME}|{
                                 CLIENT_PASSWORD}")
                    logger.info(f"Entered game as: '{CLIENT_USERNAME}'")

                elif pack[0].decode() == 'game':
                    # clear facts from previous game
                    # players = {}
                    dead_players = []
                    players_shown = False

                    # set game facts
                    game['width'] = pack[1].decode()
                    game['height'] = pack[2].decode()
                    game['playerid'] = pack[3].decode()
                    # print newgame
                    logger.info(f"GAME START: {game['width']}x{
                                game['height']}, id: {game['playerid']}")

                    # for height in game['height']:
                    #     for width in game['width']:
                    #         game_matrix.append([])

                elif pack[0].decode() == 'error':
                    logger.error(f"{pack[1].decode()}")
                elif pack[0].decode() == 'pos':
                    pos[pack[1].decode()] = pack[2].decode()
                    logger.info(f"pos: {pos}")
                    # build_matrix()
                elif pack[0].decode() == 'player':
                    players[pack[1].decode()] = pack[2].decode()
                elif pack[0].decode() == 'tick':
                    move()

                elif pack[0].decode() == 'message':
                    logger.info(pack[1].decode())
                elif pack[0].decode() == 'die':
                    pack.pop(0)
                    for player in pack:
                        dead_players.append(player)
                    print(dead_players)
                elif pack[0].decode() == 'win':
                    logger.info("You WIN!!111")

                elif pack[0].decode() == 'lose':
                    logger.info("You loose :(")

            else:
                logger.error(
                    f"{pack[0].decode()} it not a valid package type!")
        # logging.info(f"{players=}")
